# Run this script using `rake "import_data"` for default configurations.
# Run with `rake "import_data[<batches>, <batch_size>]"` specify configuration.
task :import_data, [:batches, :batch_size] => :environment do |_task, args|
  file_path = Rails.root.join("db", "data", "test.dat")

  max_iterations = (args[:batches] || 10).to_i
  batch_size = (args[:batch_size] || 10).to_i

  # Parse out headers (first row)
  headers = File.open(file_path, &:readline)

  consumers = []
  count = 0
  iterations = 0

  # Iterate line by line so we don't load the entire file into memory
  File.foreach(file_path).with_index do |line, index|
    next if index.zero? # Skip header line because we've already parsed them

    # Pull out name and SSN, and strip whitespace from name
    name = line[0...72].strip
    social_security_number = line[72...81]

    # Get tag values by splitting every 9 characters, then casting everything to an integer
    credit_tag_values = line[81...-1].chars.each_slice(9).map(&:join).map(&:to_i)

    # Hacky way of filtering out the "NAME" and "SSN" headers
    tag_headers = headers.split(" ").map do |header|
      if %w[NAME SSN].include? header
        nil
      else
        header
      end
    end.compact

    # Zip header array and value arrays together into a hash
    credit_tags = Hash[tag_headers.zip(credit_tag_values)]

    # Construct Consumer object
    consumer = Consumer.new.tap do |c|
      c.name = name
      c.ssn = social_security_number

      credit_tags.each do |key, value|
        c.credit_tags[key] = value
      end
    end

    consumers << consumer
    count += 1

    # Import in batchess
    if count == batch_size
      puts "Importing batch of #{batch_size}..."
      # Have to call .save instead of doing a single INSERT statement because the library we're using
      # to manage the EAV relationship has a callback on .save
      #
      # In practice I would use a single INSERT statement
      consumers.map(&:save!)
      puts "Done"

      consumers = []
      count = 0

      iterations += 1
      if iterations > max_iterations - 1
        exit
      end
    end
  end

  puts "Importing remaining..."
  Consumer.import!(consumers)

  puts "Done"
end
