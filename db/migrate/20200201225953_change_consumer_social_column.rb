class ChangeConsumerSocialColumn < ActiveRecord::Migration[6.0]
  def change
    rename_column :consumers, :social_security_number, :ssn

    change_column :consumers, :ssn, :text
  end
end
