class CreateConsumerCreditTags < ActiveRecord::Migration[6.0]
  def change
    create_table :consumer_credit_tags do |t|
      t.references :consumer, null: false, type: :uuid
      t.string :entry_key, null: false
      t.text :value, null: false
      t.integer :value_type, null: false
      t.boolean :symbol_key, :null => false, :default => true

      t.timestamps
    end

    add_index :consumer_credit_tags, :entry_key
    add_index :consumer_credit_tags, :value
  end
end