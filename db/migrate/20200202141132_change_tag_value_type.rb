class ChangeTagValueType < ActiveRecord::Migration[6.0]
  def change
    change_column :credit_tags, :value, "bigint USING value::bigint"
  end
end
