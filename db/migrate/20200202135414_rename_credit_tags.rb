class RenameCreditTags < ActiveRecord::Migration[6.0]
  def change
    rename_table :consumer_credit_tags, :credit_tags
  end
end
