class CreateConsumers < ActiveRecord::Migration[6.0]
  def change
    create_table :consumers, id: :uuid do |t|
      t.text :name, null: false
      t.bigint :social_security_number, null: false

      t.timestamps
    end

    add_index :consumers, :name
    add_index :consumers, :social_security_number
  end
end
