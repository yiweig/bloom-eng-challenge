# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_02_141132) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "consumers", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.text "name", null: false
    t.text "ssn", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_consumers_on_name"
    t.index ["ssn"], name: "index_consumers_on_ssn"
  end

  create_table "credit_tags", force: :cascade do |t|
    t.uuid "consumer_id", null: false
    t.string "entry_key", null: false
    t.bigint "value", null: false
    t.integer "value_type", null: false
    t.boolean "symbol_key", default: true, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["consumer_id"], name: "index_credit_tags_on_consumer_id"
    t.index ["entry_key"], name: "index_credit_tags_on_entry_key"
    t.index ["value"], name: "index_credit_tags_on_value"
  end

end
