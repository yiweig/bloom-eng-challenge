# == Schema Information
#
# Table name: credit_tags
#
#  id          :bigint           not null, primary key
#  entry_key   :string           not null
#  symbol_key  :boolean          default(TRUE), not null
#  value       :bigint           not null
#  value_type  :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  consumer_id :uuid             not null
#
# Indexes
#
#  index_credit_tags_on_consumer_id  (consumer_id)
#  index_credit_tags_on_entry_key    (entry_key)
#  index_credit_tags_on_value        (value)
#

class CreditTag < ApplicationRecord

end
