# == Schema Information
#
# Table name: consumers
#
#  id         :uuid             not null, primary key
#  name       :text             not null
#  ssn        :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_consumers_on_name  (name)
#  index_consumers_on_ssn   (ssn)
#

class Consumer < ApplicationRecord
  eav_hash_for :credit_tags, table_name: :credit_tags
end
