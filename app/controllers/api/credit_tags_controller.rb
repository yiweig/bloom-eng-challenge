module Api
  class CreditTagsController < ApplicationController
    def index
      tag = params[:tag]
      credit_tags = CreditTag.where(entry_key: tag).where("value > 0").pluck(:value)

      render json: {
        tag: tag,
        stats: {
          mean: mean(credit_tags),
          median: median(credit_tags),
          standard_deviation: standard_deviation(credit_tags)
        }
      }
    end

    private

    # Statistics code courtesy of https://www.chrisjmendez.com/2018/10/14/mean-median-mode-standard-deviation/

    def mean(array)
      array.reduce(0) { |sum, x| sum + x } / array.size.to_f
    end

    # If the array has an odd number, then pick the one in the middle
    # If the array size is even, then we must calculate the mean of the two middle.
    def median(array)
      return nil if array.empty?
      array = array.sort
      middle = array.size / 2
      if array.size % 2 == 1
        array[middle]
      else
        mean(array[middle - 1..middle])
      end
    end

    def standard_deviation(array)
      m = mean(array)
      variance = array.reduce(0) { |variance, x| variance + (x - m) ** 2 }
      Math.sqrt(variance / (array.size - 1))
    end
  end
end