module Api
  class ConsumersController < ApplicationController
    def index
      uuid = params[:id]
      consumer = Consumer.find_by(id: uuid)

      # 404 if user is not found
      if consumer.nil?
        return render json: [], status: 404
      end

      render json: {
        user_id: uuid,
        credit_tags: consumer.credit_tags.as_hash
      }
    end
  end
end
