# Bloom Engineering Challenge

### Version Requirements
```
> $ ruby --version
ruby 2.7.0p0 (2019-12-25 revision 647ee6f091) [x86_64-darwin19]

> $ rails version
Rails 6.0.2.1

> $ postgres --version
postgres (PostgreSQL) 11.2
```

### Setup
These steps assume that:
 
- Postgres is up and running on the default port (5432)
- Sample data is decompressed at `db/data/test.dat`
- Ruby/Rails is installed, and you've run `bundle install` to install/update all gems.

Run `rails db:setup` to get the DB prepared from an uninitialized state.

Run `rails db:reset` to start from a clean slate if the DB already exists.

Use the `import_data.rake` script to import sample data. Note that this is _really_ slow at the moment because it's not optimized for such a huge dataset.

By default the script imports records in batches of 100 records, up to 10 batches. 
It is possible to configure the script to import more data by overriding the default parameters:

```
rake "import_data[<batches>, <batch_size>]"

# Example for 50 batches of 25 records:
rake "import_data[50, 25]" 
``` 

Run the server via:
```
rails server
```

### APIs

There are API endpoints available at the default path: `http://localhost:3000`

- `/api/consumers/?id=<id>` : takes a consumer's UUID as a query param, and returns all of their credit tags and values.
  - Example return: 
  ```
  {
      "user_id": "00668792-ea08-4476-b700-d12c8e6ac7e5",
      "credit_tags": {
          "X0001": -4,
          "X0002": 687092,
          "X0003": -2,
          "X0004": 790433,
          "X0005": 733825,
          ...
      }
  }
  ```
- `/api/credit_tags/?tag=<tag>` : takes a credit tag (eg. `X0123`) as a query param and returns an object with statistics for all positive values of that tag.
  - Example return: 
  ```
  {
      "tag": "X0123",
      "stats": {
          "mean": 539367.5652173914,
          "median": 480721.5,
          "standard_deviation": 272072.81318521686
      }
  }
  ```   